import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, ScrollView, FlatList } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import BookInput from './components/BooksInput';
import BookItem from './components/BooksItem';
import MovieInput from './components/MovieInput';
import MovieItem from './components/MovieItem';
export default function App() {


  const [books, setBooks] = useState([]);
  const [movies, setMovies] = useState([]);
  const [isAddMode, setAddMode] = useState(false);

  const addBooks = bookData => {
    setBooks(currentBook => [...currentBook, { id: Math.random().toString(), value: bookData }]);
    setAddMode(false);
  };
  const addMovies = movieData => {
    setMovies(curretMovie => [...curretMovie, { id: Math.random().toString(), value: movieData }]);
    setAddMode(false);
  };
  const removeBook = bookId => {
    setBooks(currentBook => {
      return currentBook.filter((book) => book.id != bookId)
    });
  };

  const removeMovies = movieId => {
    setBooks(currentMovie => {
      return currentMovie.filter((movie) => movie.id != movieId)
    });
  };
  const cancelData = () => {
    setAddMode(false);
  };
  return (
    <View style={styles.container}>
      <Button title="Add Books" onPress={() => setAddMode(true)} />
      <Button title="Add Movies" onPress={() => setAddMode(true)} />

      <BookInput visible={isAddMode} onAddBook={addBooks} onCancel={cancelData} />
      <MovieInput visible={isAddMode} onAddMovie={addMovies} onCancel={cancelData} />
      <FlatList data={books}
        keyExtractor={(item, index) => item.id}
        renderItem={data => <BookItem id={data.item.id} onDelete={removeBook} title={data.item.value} />}
      />

      <FlatList data={movies}
        keyExtractor={(item, index) => item.id}
        renderItem={data => <MovieItem id={data.item.id} onDelete={removeMovies} title={data.item.value} />}
      />

    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 50,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }

});
