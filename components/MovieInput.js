import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet, Modal } from 'react-native';


const MovieInput = props => {
    const [enteredMovie, setEnteredMovie] = useState('');
    const movieInput = (enteredText) => {
        setEnteredMovie(enteredText);
    };

    const addMovie = () => {
        props.onAddMovie(enteredBook);
        setEnteredMovie('');
    };


    return (
        <Modal visible={props.visible} animationType="slide">
            <View style={styles.inputContainer}>
                <TextInput placeholder="Enter Name"
                    style={styles.container}
                    onChangeText={movieInput}
                    value={enteredMovie} />
                <View style={styles.buttonContainer}>
                    <Button title="CANCEL" color="red" onPress={props.onCancel} />
                    <Button title="ADD" onPress={addMovie} />
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    inputContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        width: '80%',
        borderColor: 'black',
        borderWidth: 1,
        marginBottom: 10
    },

    buttonContainer: {
        flexDirection: 'row-reverse',
        justifyContent: 'center'
    }

});
export default MovieInput;